import React, {Component} from 'react';

class CardLinks extends Component {
  render() {
    const profileLinks = ['twitter', 'linkedin', 'dribbble', 'facebook'];

    const linksList = profileLinks.map((link) => {
      return (
        <li>
          <a href = "#">
            <i className = {`fa fa-${link}`}></i>
          </a>
        </li>
      )
    });

    return(
      <div className = 'card-social-links'>
        <ul className="social-links">
          {linksList}
        </ul>
      </div>
    )
  }
}

export default CardLinks;
